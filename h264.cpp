#include <string>
#include <iterator>
#include <iostream>
#include <algorithm>
#include <array>

std::array< std::array< float, 4 >, 4> lumaPredict4by4(std::array< std::array< float, 4 >, 4> ,  std::array<float,13>);
std::array< std::array< float, 8>, 8> chromapredict8by8(std::array< std::array< float, 8>, 8>, std::array<float,8> , std::array<float,8> );
std::array< std::array< float, 16>, 16> lumapredict16by16(std::array< std::array< float, 16>, 16>, std::array<float,16>, std::array<float,16> );
float calcSAE4by4(   std::array< std::array< float, 4 >, 4>     ,  std::array< std::array< float, 4 >, 4>  );
float calcSAE8by8(   std::array< std::array< float, 8 >, 8>     ,  std::array< std::array< float, 8 >, 8>  );
float calcSAE16by16(   std::array< std::array< float, 16 >, 16>     ,  std::array< std::array< float, 16 >, 16>  );
int print4by4(std::array< std::array< float, 4 >, 4>);
int print8by8(std::array< std::array< float, 8 >, 8>);
int print16by16(std::array< std::array< float, 16 >, 16>);

int main()
{

    std::array< std::array<float,4> , 4> a;
    std::array< std::array<float,4> , 4> b;
    int i, j;
    for(i = 0; i < 4; i++){
        for(j = 0; j < 4; j++){
            a[i][j] = i + j*j*j;
        }
    };

    for(i = 0; i < 4; i++){
        for(j = 0; j < 4; j++){
            b[i][j] = i + j*j*j + 1;
        }
    };
    std::cout<<"img: \n";
    for(i = 0; i < 4; i++){
        for(j = 0; j < 4; j++){
            std::cout<<a[i][j]<<' ';
        }
        std::cout<<'\n';
    };


    std::array< float, 13 > neighbours  = {0,1,2,3,4,5,6,7,8,9,10,11,12};

    std::cout<<"\nneighbours:\n";
    std::cout<<' '<<neighbours[12];
    for(i = 0; i< 8; i++)
        std::cout<<' '<<neighbours[i];
    for(i = 8; i < 12; i++)
        std::cout<<"\n "<<neighbours[i];
    std::cout<<"\n";



    print4by4(lumaPredict4by4(a,neighbours));
//    print4by4(a);

    std::cout<<sizeof(a)/sizeof(float)<<"\n";

};

std::array< std::array< float, 4 >, 4> lumaPredict4by4(std::array< std::array< float, 4 >, 4>  img, std::array< float, 13 > neighbours){
//takes as inputs two arrays: one representing the 4x4 luma block, and the other the neighbours of this macroblock.
//tests which of the 9 modes is better
    
// the neighbours array follows the standard as [ABCEDFGH IJKL M] [horizontal vertical corner] 

    int i,j;
    int indexminSAE = 0;
    std::array< float, 9> SAE;
    std::array<   std::array< std::array< float, 4 >, 4>, 9> res;

//mode 0: vertical
    for(i = 0; i < 4; i++)
        res[0][i][0] = img[i][0] - neighbours[0];
    for(i = 0; i < 4; i++)
        res[0][i][1] = img[i][1] - neighbours[1];
    for(i = 0; i < 4; i++)
        res[0][i][2] = img[i][2] - neighbours[2];
    for(i = 0; i < 4; i++)
        res[0][i][3] = img[i][3] - neighbours[3];        

//mode 1: horizontal 
    for(i = 0; i < 4; i++)
        res[1][0][i] = img[0][i] - neighbours[8];   
    for(i = 0; i < 4; i++)
        res[1][1][i] = img[0][i] - neighbours[9];    
    for(i = 0; i < 4; i++)
        res[1][2][i] = img[0][i] - neighbours[10];    
    for(i = 0; i < 4; i++)
        res[1][3][i] = img[0][i] - neighbours[11];    
 
//mode 2: DC 
    float averageNeighbours = 0;
    for(i = 0; i < 13; i ++)
        averageNeighbours = averageNeighbours + neighbours[i];
    averageNeighbours = averageNeighbours/13;
 
   for(i = 0; i < 4; i++){
        for(j = 0; j < 4; j++){
            res[2][i][j] = img[i][j] - averageNeighbours;

        }
    };

//mode 3: diagonal esquerda baixa
    //a = round(B/2 + A/4 + C/4)
    res[3][0][0] = img[0][0] - round(neighbours[1]/2 + neighbours[0]/4 +neighbours[2]/4);   
    //b,e = round(C/2 + B/4 + D/4)
    res[3][0][1] = img[0][1] - round(neighbours[2]/2 + neighbours[1]/4 +neighbours[3]/4); 
    res[3][1][0] = img[1][0] - round(neighbours[2]/2 + neighbours[1]/4 +neighbours[3]/4); 
    //c,f,i = round(D/2 + C/4 + E/4)
    res[3][0][2] = img[0][2] - round(neighbours[3]/2 + neighbours[2]/4 +neighbours[4]/4); 
    res[3][1][1] = img[1][1] - round(neighbours[3]/2 + neighbours[2]/4 +neighbours[4]/4); 
    res[3][2][0] = img[2][0] - round(neighbours[3]/2 + neighbours[2]/4 +neighbours[4]/4); 
    //d,g,j,m = round(E/2 + D/4 + F/4)
    res[3][0][3] = img[0][3] - round(neighbours[4]/2 + neighbours[3]/4 +neighbours[5]/4); 
    res[3][1][2] = img[1][2] - round(neighbours[4]/2 + neighbours[3]/4 +neighbours[5]/4); 
    res[3][2][1] = img[2][1] - round(neighbours[4]/2 + neighbours[3]/4 +neighbours[5]/4); 
    res[3][3][0] = img[3][0] - round(neighbours[4]/2 + neighbours[3]/4 +neighbours[5]/4); 
    //h,k,n = round(F/2 + E/4 + G/4)
    res[3][1][3] = img[1][3] - round(neighbours[5]/2 + neighbours[4]/4 +neighbours[6]/4); 
    res[3][2][2] = img[2][2] - round(neighbours[5]/2 + neighbours[4]/4 +neighbours[6]/4); 
    res[3][3][1] = img[3][1] - round(neighbours[5]/2 + neighbours[4]/4 +neighbours[6]/4); 
    //l,o = round(G/2 + F/4 + H/4)
    res[3][2][3] = img[2][3] - round(neighbours[6]/2 + neighbours[5]/4 +neighbours[7]/4); 
    res[3][3][2] = img[3][2] - round(neighbours[6]/2 + neighbours[5]/4 +neighbours[7]/4); 
    //p = round(H/2 + G/4 + H/4)
    res[3][3][3] = img[3][3] - round(neighbours[7]/2 + neighbours[6]/4 +neighbours[7]/4);

//mode 4: diagonal direita baixa
    //d = round(C/2 + B/4 + D/4)
    res[4][0][3] = img[0][3] - round(neighbours[2]/2 + neighbours[1]/4 +neighbours[3]/4);   
    //c,h = round(B/2 + A/4 + C/4)
    res[4][0][2] = img[0][2] - round(neighbours[1]/2 + neighbours[0]/4 +neighbours[2]/4);   
    res[4][1][3] = img[1][3] - round(neighbours[1]/2 + neighbours[0]/4 +neighbours[2]/4);   
    //b,g,l = round(A/2 + M/4 + B/4)  
    res[4][0][0] = img[0][1] - round(neighbours[0]/2 + neighbours[12]/4 +neighbours[1]/4);   
    res[4][1][2] = img[1][2] - round(neighbours[0]/2 + neighbours[12]/4 +neighbours[1]/4);   
    res[4][2][2] = img[2][3] - round(neighbours[0]/2 + neighbours[12]/4 +neighbours[1]/4);   
    //a,f,k,p = round(M/2 + A/4 + I/4)  
    res[4][0][0] = img[0][0] - round(neighbours[12]/2 + neighbours[0]/4 +neighbours[8]/4);   
    res[4][1][1] = img[1][1] - round(neighbours[12]/2 + neighbours[0]/4 +neighbours[8]/4);   
    res[4][2][2] = img[2][2] - round(neighbours[12]/2 + neighbours[0]/4 +neighbours[8]/4);   
    res[4][3][3] = img[3][3] - round(neighbours[12]/2 + neighbours[0]/4 +neighbours[8]/4);   
    //e,j,o = round(I/2 + M/4 + J/4)  
    res[4][1][0] = img[1][0] - round(neighbours[8]/2 + neighbours[12]/4 +neighbours[9]/4);   
    res[4][2][1] = img[2][1] - round(neighbours[8]/2 + neighbours[12]/4 +neighbours[9]/4);   
    res[4][3][2] = img[3][2] - round(neighbours[8]/2 + neighbours[12]/4 +neighbours[9]/4);   
    //i,n = round(J/2 + I/4 + K/4)  
    res[4][2][0] = img[2][0] - round(neighbours[9]/2 + neighbours[8]/4 +neighbours[10]/4); 
    res[4][3][1] = img[3][1] - round(neighbours[9]/2 + neighbours[8]/4 +neighbours[10]/4); 
    //m = round(K/2 + J/4 + L/4)  
    res[4][3][0] = img[2][0] - round(neighbours[10]/2 + neighbours[9]/4 +neighbours[11]/4);


//mode 5: Vertical Direita
    //a,j = M/2 + A/2
    res[5][0][0] = img[0][0] - (neighbours[12] + neighbours[0])/2;
	res[5][2][1] = img[2][1] - (neighbours[12] + neighbours[0])/2;
    //b,k = B/2 + A/2
    res[5][0][1] = img[0][1] - (neighbours[0] + neighbours[1])/2;
	res[5][2][2] = img[2][2] - (neighbours[0] + neighbours[1])/2;
    //c,l = B/2 + C/2
    res[5][0][2] = img[0][2] - (neighbours[2] + neighbours[1])/2;
	res[5][2][3] = img[2][3] - (neighbours[2] + neighbours[1])/2;
    // d = C/2 + D/2 
    res[5][0][3] = img[0][3] - (neighbours[2] + neighbours[3])/2;
    // e,n = M/2 + A/4 + I/4
    res[5][1][0] = img[1][0] - (neighbours[12]/2 + neighbours[0]/4 + neighbours[8]/4);
    res[5][3][1] = img[3][1] - (neighbours[12]/2 + neighbours[0]/4 + neighbours[8]/4);
    // f,o = M/4 + A/2 + B/4
    res[5][1][1] = img[1][1] - (neighbours[12]/4 + neighbours[0]/2 + neighbours[1]/4);
    res[5][3][2] = img[3][2] - (neighbours[12]/4 + neighbours[0]/2 + neighbours[1]/4);
    // g,p = A/4 + B/2 + C/4
    res[5][1][2] = img[1][2] - (neighbours[0]/4 + neighbours[1]/2 + neighbours[2]/4);
    res[5][3][3] = img[3][3] - (neighbours[0]/4 + neighbours[1]/2 + neighbours[2]/4);
    // h = D/4 + B/4 + C/2
    res[5][1][3] = img[1][3] - (neighbours[1]/4 + neighbours[2]/2 + neighbours[3]/4);
    // i = M/4 + I/2 + J/4
    res[5][2][0] = img[2][0] - (neighbours[12]/4 + neighbours[8]/2 + neighbours[9]/4);
    // m = I/4 + J/2 + K/4
    res[5][3][0] = img[3][0] - (neighbours[8]/4 + neighbours[9]/2 + neighbours[10]/4);




//mode 6: horizontal baixa
	// a,g =  I/2 + M/2
	res[6][0][0] = img[0][0] - (neighbours[8] + neighbours[12])/2;
	res[6][1][2] = img[1][2] - (neighbours[8] + neighbours[12])/2;
	// b,h = M/2 + A/4 + I/4
	res[6][0][1] = img[0][1] - (neighbours[12]/2 + neighbours[8]/4  +  neighbours[0]/4);
	res[6][1][3] = img[1][3] - (neighbours[12]/2 + neighbours[8]/4  +  neighbours[0]/4); 
	// c = M/4 + A/2 + B/4
	res[6][0][2] = img[0][2] - (neighbours[12]/4 + neighbours[0]/2  +  neighbours[1]/4);
	// d = A/4 + B/2 + C/4
    res[6][0][3] = img[0][3] - (neighbours[0]/4 + neighbours[1]/2  +  neighbours[2]/4);
	// e,k = I/2 + J/2
 	res[6][1][0] = img[1][0] - (neighbours[8] + neighbours[9])/2;
    res[6][2][2] = img[2][2] - (neighbours[8] + neighbours[9])/2;
	// f, l = M/4 + I/2 + J/4
    res[6][1][1] = img[1][1] - (neighbours[12]/4 + neighbours[8]/2  +  neighbours[9]/4);
    res[6][2][3] = img[2][3] - (neighbours[12]/4 + neighbours[8]/2  +  neighbours[9]/4);
	// i,o = (J+K)/2
  	res[6][2][0] = img[2][0] - (neighbours[9] + neighbours[10])/2;
  	res[6][3][2] = img[3][2] - (neighbours[9] + neighbours[10])/2;
    //j, p = J/2 + I/4 + K/4
    res[6][2][1] = img[2][1] - (neighbours[8]/4 + neighbours[9]/2  +  neighbours[10]/4);
    res[6][3][3] = img[3][3] - (neighbours[8]/4 + neighbours[9]/2  +  neighbours[10]/4);
    // m = (K + L) /2
  	res[6][3][0] = img[3][0] - (neighbours[11] + neighbours[10])/2;
    // n = J/4 + K/2 +L/4
    res[6][3][1] = img[3][1] - (neighbours[10]/4 + neighbours[10]/2  +  neighbours[11]/4);

    //modo 7: Vertical Left
    // a = A/2 + B/2
 	res[7][0][0] = img[0][0] - (neighbours[0] + neighbours[1])/2;
    // b,i = C/2 + B/2
 	res[7][0][1] = img[0][1] - (neighbours[2] + neighbours[1])/2;
 	res[7][2][0] = img[2][0] - (neighbours[2] + neighbours[1])/2;
    // c,j = C/2 + D/2
 	res[7][0][2] = img[0][2] - (neighbours[2] + neighbours[3])/2;
 	res[7][2][1] = img[2][1] - (neighbours[2] + neighbours[3])/2;
    // d,k = E/2 + D/2
 	res[7][0][3] = img[0][3] - (neighbours[4] + neighbours[3])/2;
 	res[7][2][2] = img[2][2] - (neighbours[4] + neighbours[3])/2;
    // e = (A + 2B + C)/4
    res[7][1][0] = img[1][0] - (neighbours[0] + 2*neighbours[1] + neighbours[2] )/4;
    // f,m = (B + 2C + D)/4
    res[7][1][1] = img[1][1] - (neighbours[1] + 2*neighbours[2] + neighbours[3] )/4;    
    res[7][3][0] = img[3][0] - (neighbours[1] + 2*neighbours[2] + neighbours[3] )/4;
    // g,n = (C + 2D + E)/4
    res[7][1][2] = img[1][2] - (neighbours[2] + 2*neighbours[3] + neighbours[4] )/4;    
    res[7][3][1] = img[3][1] - (neighbours[2] + 2*neighbours[3] + neighbours[4] )/4;
    // h,o  = (D + 2E + F)/4
    res[7][1][3] = img[1][3] - (neighbours[3] + 2*neighbours[4] + neighbours[5] )/4;    
    res[7][3][2] = img[3][2] - (neighbours[3] + 2*neighbours[4] + neighbours[5] )/4;
    // l = E/2 + F/2
 	res[7][2][3] = img[2][3] - (neighbours[4] + neighbours[5])/2;
    // p  = (E + 2F + G)/4
    res[7][3][3] = img[3][3] - (neighbours[4] + 2*neighbours[5] + neighbours[6] )/4;    

    //mode 8: horizontal-up
    // a = (I + J)/2
 	res[8][0][0] = img[0][0] - (neighbours[8] + neighbours[9])/2;
    // b  = (I + 2J + K)/4
    res[8][0][1] = img[0][1] - (neighbours[8] + 2*neighbours[9] + neighbours[10] )/4;    
    // c,e = (K + J)/2
 	res[8][0][2] = img[0][2] - (neighbours[10] + neighbours[9])/2;
 	res[8][1][0] = img[1][0] - (neighbours[10] + neighbours[9])/2;
    // d,f  = (J + 2K + L)/4
    res[8][0][3] = img[0][3] - (neighbours[9] + 2*neighbours[10] + neighbours[11] )/4; 
    res[8][1][1] = img[1][1] - (neighbours[9] + 2*neighbours[10] + neighbours[11] )/4; 
    // g,i = (K + L)/2
 	res[8][1][2] = img[1][2] - (neighbours[10] + neighbours[11])/2;
 	res[8][2][0] = img[2][0] - (neighbours[10] + neighbours[11])/2;
    // h,j = K/4 + 3L/4
 	res[8][1][3] = img[1][3] - (neighbours[10] + 3*neighbours[11])/4;
 	res[8][2][1] = img[2][1] - (neighbours[10] + 3*neighbours[11])/4;
    // k,l,m,n,o,p = L
 	res[8][2][2] = img[2][2] - neighbours[11];
 	res[8][2][3] = img[2][3] - neighbours[11];
 	res[8][3][0] = img[3][0] - neighbours[11];
 	res[8][3][1] = img[3][1] - neighbours[11];
 	res[8][3][2] = img[3][2] - neighbours[11];
 	res[8][3][3] = img[3][3] - neighbours[11];




    for(i = 0; i < 9; i++){
        std::cout<<"res["<<i<<"]";
        print4by4(res[i]);
    };
    for(i = 0; i < 9; i++){
        SAE[i] = calcSAE4by4(img,res[i]);
        std::cout<<"SAE["<<i<<"]: "<<SAE[i]<<std::endl;
    };

    for(i = 0; i < SAE.size() ; i++)
        if (SAE[i] < SAE[indexminSAE])
            indexminSAE = i;
    std::cout<<"minSAE:"<<indexminSAE;


    return res[indexminSAE];

};




std::array< std::array< float, 8>, 8> chromapredict8by8(std::array< std::array< float, 8>, 8> img, std::array<float,8> top, std::array<float,8> left){

    int i, j, indexminSAE;
    float sumDC = 0;
    std::array<float, 4 > SAE;
    std::array<std::array<std::array<float , 8>, 8>, 4> res;
    //mode 0: DC, mode1:horizontal; mode 2: vertical;
    for(i = 0; i < 8; i++)
        sumDC += top[i] + left[i];
    for(i = 0; i < 8; i++){
        for(j = 0; j < 8; j++){
            res[0][i][j] = img[i][j] - sumDC/16;
            res[1][i][j] = img[i][j] - left[i];
            res[2][i][j] = img[i][j] - top[j];
        };
    };
    
    //mode 3: planar
    float iTopSum, iLeftSum;
    iTopSum = 0;
    iLeftSum = 0;

    for (i = 0 ; i < 4 ; i ++) {
        iTopSum += (i + 1) * (top[4 + i] - top[2 - i]);
        iLeftSum += (i + 1) * (left[4 + i] - left[2 - i]);
    };

    for (i = 0 ; i < 8 ; i ++) {
        for (j = 0 ; j < 8 ; j ++) {
        res[3][i][j] = img[i][j] - ((left[7]+top[7])/2 + 17*(j-3)*iTopSum/(32*32) + 17*(i-3)*iLeftSum/(32*32));
        };
    };


    for( i = 0; i < 4; i++){
        std::cout<<"res["<<i<<"]\n";
        print8by8(res[i]); 
        SAE[i] = calcSAE8by8(img, res[i]);
    };

    for( i = 1; i < 4; i++){
        if (SAE[i] < SAE[indexminSAE]){
            indexminSAE = i;
        };   
    };
        
    return res[indexminSAE]; 



}





std::array< std::array< float, 16>, 16> lumapredict16by16(std::array< std::array< float, 16>, 16> img, std::array<float,16> top, std::array<float,16> left){

    std::array<std::array< std::array< float, 16>, 16>,4> res;
    int i,j = 0;
    float sumV = 0;
    float sumH = 0;    
    float iTopSum = 0;
    float iLeftSum = 0;
    
    std::array< float, 4> SAE;

    for(i = 0; i < 16; i++){
        sumV = sumV + top[i]; 
        sumH = sumH + left[i]; 
};    

    //mode 0,1,2: Vertical, Horizontal, DC
    for(i = 0; i < 16; i++){
        for(j = 0; j < 16; j++){
            res[0][i][j] = img[i][j] - (top[j]);
            res[1][i][j] = img[i][j] - (left[i]);
            res[2][i][j] = img[i][j] - (sumV+sumH)/32;         
        };
    };

    //mode 3: planar

  for (i = 0 ; i < 8 ; i ++) {
    iTopSum += (i + 1) * (top[8 + i] - top[6 - i]);
    iLeftSum += (i + 1) * (left[8 + i] - left[6 - i] );
  }

    for(i = 0; i < 16; i++){
        for(j = 0; j < 16; j++){
            res[3][i][j] = img[i][j] - (   (left[15]+top[15])/2 + 5*(iTopSum/(64*32))*(j-7) + 5*(i-7)*iLeftSum/(64*32)     );
        };
    };


    for( i = 0; i < 4; i++){
        std::cout<<"res["<<i<<"]\n";
        print16by16(res[i]); 
        SAE[i] = calcSAE16by16(img, res[i]);
    };

    int indexminSAE = 0;
    float minSAE = SAE[0];
    for( i = 1; i < 4; i++){
        if (SAE[i] < minSAE){
            indexminSAE = i;
            minSAE = SAE[i];
        };   
    };
        
    return res[indexminSAE]; 
};






float calcSAE4by4(std::array< std::array< float, 4 >, 4>  img, std::array< std::array< float, 4 >, 4> res){
// inputs two blocks of size 4by4 and outputs the sum of absolute values for each pixel
    int i,j;
    float SAE = 0;
    for(i = 0; i < 4; i++){
        for(j = 0; j < 4; j++){
            SAE = SAE + abs(img[i][j] - res[i][j]);
        };
    };
    return SAE;
};

float calcSAE8by8(std::array< std::array< float, 8 >, 8>  img, std::array< std::array< float, 8 >, 8> res){
    int i,j;
    float SAE = 0;
    for(i = 0; i < 8; i++){
        for(j = 0; j < 8; j++){
            SAE = SAE + abs(img[i][j] - res[i][j]);
        };
    };
    return SAE;
};

float calcSAE16by16(std::array< std::array< float, 16 >, 16>  img, std::array< std::array< float, 16 >, 16> res){
// inputs two blocks of size 16x16 and outputs the sum of absolute values for each pixel
    int i,j;
    float SAE = 0;
    for(i = 0; i < 16; i++){
        for(j = 0; j < 16; j++){
            SAE = SAE + abs(img[i][j] - res[i][j]);
        };
    };
    return SAE;
};




int print4by4(std::array< std::array< float, 4 >, 4> a){
// prints a 4 by 4 array on cout
    int i,j;
    for(i = 0; i < 4; i++){
        std::cout<<'\n';
        for(j = 0; j < 4; j++)
            std::cout<<' '<<a[i][j];
    };
    std::cout<<"\n\n";
    return 0;    
};


int print8by8(std::array< std::array< float, 8 >, 8> a){
    int i,j;
    for(i = 0; i < 8; i++){
        std::cout<<'\n';
        for(j = 0; j < 8; j++)
            std::cout<<' '<<a[i][j];
    };
    std::cout<<"\n\n";
    return 0;    
};

int print16by16(std::array< std::array< float, 16 >, 16> a){
// prints a 16 by 16 array on cout
    int i,j;
    for(i = 0; i < 16; i++){
        std::cout<<'\n';
        for(j = 0; j < 16; j++)
            std::cout<<' '<<a[i][j];
    };
    std::cout<<"\n\n";
    return 0;    
};

